﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace WordCount
{
    class WordParser
    {
        readonly private string fileName;
        public string FileName { get { return fileName; } }
        private bool running;

        public WordParser(string fileName)
        {
            this.fileName = fileName;
            this.running = true;
        }

        // progressCallback takes argument in the range [0, 100] to indicate how many % of the file have been processed
        public Dictionary<string, uint> ParseFrequency(Action<int> progressCallback)
        {
            running = true;
            long size = new FileInfo(fileName).Length;
            long readSize = 0;
            int progress;

            Dictionary<string, uint> dict = new Dictionary<string, uint>();
            using (StreamReader stream = File.OpenText(fileName))
            {
                string line;
                while ((line = stream.ReadLine()) != null && running)
                {
                    foreach (var word in line.Split(null))
                    {
                        if (string.IsNullOrWhiteSpace(word))
                            continue;
                        if (dict.ContainsKey(word))
                            dict[word]++;
                        else
                            dict[word] = 1;
                    }
                    if (progressCallback != null)
                    {
                        // calculation not 100% accurate, but fast
                        readSize += Encoding.ASCII.GetByteCount(line);
                        progress = (int)(((double)readSize / (double)size) * 100.0);
                        progressCallback.Invoke(progress);
                    }
                }
            }

            return (running) ? dict : new Dictionary<string, uint>(); ;
        }

        public void Cancel()
        {
            this.running = false;
        }
    }
}
