﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace WordCount
{
    public partial class FormWordCount : Form
    {
        private WordParser parser;
        private Thread loadThread;

        public FormWordCount()
        {
            InitializeComponent();
        }

        private void FillTable()
        {
            Dictionary<string, uint> dict;
            try
            {
                dict = parser.ParseFrequency(SetProgress);
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not open '" + parser.FileName + "': " + e.Message);
                return;
            }

            dataGridView.Invoke(new Action(() =>
            {
                dataGridView.DataSource = dict.OrderByDescending(e => e.Value).ToArray();
                if (dataGridView.Columns["Key"] != null)
                    dataGridView.Columns["Key"].HeaderText = "Word";
                if (dataGridView.Columns["Value"] != null)
                    dataGridView.Columns["Value"].HeaderText = "Count";
            }));
            progressBar.Invoke(new Action(() =>
            {
                progressBar.Value = 100;
            }));
        }

        private void ButtonOpenClick(Object sender, EventArgs e)
        {
            ToggleButtons();
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Word files (*.txt)|*.txt";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    labelFilename.Text = openFileDialog.FileName;
                    parser = new WordParser(openFileDialog.FileName);
                    loadThread = new Thread(() =>
                    {
                        FillTable();
                        Invoke(new Action(ToggleButtons));
                    });
                    loadThread.Start();
                }
                else
                {
                    ToggleButtons();
                }
            }
        }

        private void ToggleButtons()
        {
            buttonOpen.Enabled = !buttonOpen.Enabled;
            buttonCancel.Enabled = !buttonCancel.Enabled;
        }

        private void ButtonCancelClick(Object sender, EventArgs e)
        {
            parser.Cancel();
        }

        private void SetProgress(int progress)
        {
            progressBar.Invoke(new Action(() => { progressBar.Value = progress; }));
        }
    }
}
